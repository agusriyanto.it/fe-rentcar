import axios from "axios"
import { useState } from "react"
import { useNavigate } from "react-router-dom"

export default function AuthUser(){
    const navigate = useNavigate()

    
    const API_URL = axios.create({
        baseURL:'http://127.0.0.1:8000/api/',
        headers:{
            "Content-Type" : "aplication/json",
        }
    })

    return {
        API_URL
    }
}