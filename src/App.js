import { BrowserRouter,Routes, Route } from 'react-router-dom';
import Category from './Pages/Category/Category';
import Home from './Pages/Home';
import Product from './Pages/Product/Product';
import Nopage from './Pages/Nopage';
import Navigasi from './Pages/Navigasi';
import Transaction from './Pages/Transaction/Transactions';
import AddCategory from './Pages/Category/AddCategory';
import EditCategory from './Pages/Category/EditCategory';
import AddProduct from './Pages/Product/AddProduct';
import EditProduct from './Components/Product/editProduct';
import AuthComp from './Components/Auth/AuthComp';
import AuthUser from './Utils/constants';
import Register from './Pages/Auth/Register';


function App() {

  return (
    
      <div className='App'>
        {
          localStorage.getItem('token') ? <Navigasi /> : null
        }
        
        <Routes> 
          <Route exact path="/" element={<Home />} />
          <Route path="/category" element={<Category />} />
          <Route path="/product" element={<Product />} />
          <Route path="/transaction" element={<Transaction />} />
          <Route path="/add-caty" element={<AddCategory />} />
          <Route path="/edit-caty/:id" element={<EditCategory />} />
          <Route path="/add-product/" element={<AddProduct />} />
          <Route path="/edit-prod/:id" element={<EditProduct />} />
          <Route path="/login" element={<AuthComp />} />
          <Route path='/register' element={<Register />} />
          <Route path="*" element={<Nopage />} />
          
        </Routes>
      </div>       
  );
}

export default App;
