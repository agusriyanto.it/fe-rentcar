import {Link, useNavigate,Router} from 'react-router-dom'
import React, {useEffect, useState} from 'react'
import axios from 'axios'
import { Bar } from 'react-chartjs-2'
import Chart from 'chart.js/auto';

const HomeComponents = ({unAuth}) => {
    const [ccaty, setCaty] = useState([])
    const [cprod, setProd] = useState([])
    const navigate = useNavigate()

    useEffect(() => {
        if(!localStorage.getItem('token')){
            navigate('/login')
        }
        axios.get('http://127.0.0.1:8000/api/product')
        .then(response => {
            setCaty(response.data.data['ccount'])
            setProd(response.data.data['pcount'])
            console.log(response.data.data['pcount'])
        })
        .catch(error => {
            console.error('Error Fetching Data : ', error)
        })
    }, [])

    const deleteCaty = (id) => {
        axios.delete('http://127.0.0.1:8000/api/inv-caty/${id}')
        .then(response => {
            console.log('data telah dihapus')
        })
        .catch(error => {
            console.error('Gagal Menghapus data :', error)
        })
    }

    const UpdateCaty = (id) => {
        const navigate = useNavigate()

        axios.get('http://127.0.0.1:8000/api/inv-caty/${id}')
        .then(response => {
            navigate('/edit-caty')
        })
        .catch(error => {
            console.log('error edit data : ', error)
        })
    }

    const datas = {
        labels: ['Januari', 'Februari', 'Maret', 'April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'],
        datasets: [
          {
            label: 'Data Chart',
            data: [10, 15, 30, 33, 31,23,45,23,33,12,27,55],
            backgroundColor: 'rgba(75,192,192,0.6)',
            borderColor: 'rgba(75,192,192,1)',
            borderWidth: 1,
          },
        ],
      };
    
      const options = {
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true,
              },
            },
          ],
        },
      };

    return (
    <div class="container">
        <div class="row ">
            <div class="col-md-12" style={{marginTop: '20px'}}>
                <div class="row">
                    <div className="col-md-4">
                        <div class="card">
                            {/* <img src="{}" class="card-img-top" alt="..." /> */}
                            <div class="card-body">
                                <h5 class="card-title">Jumlah Produk</h5>
                                <h2 class="card-text"> {cprod}</h2>
                                <Link to={'/product'} class="btn btn-primary">Selengkapnya</Link>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div class="card">
                            {/* <img src="{}" class="card-img-top" alt="..." /> */}
                            <div class="card-body">
                                <h5 class="card-title">Jumlah Category</h5>
                                <h2 class="card-text"> {ccaty}</h2>
                                <Link to={'/category'} class="btn btn-primary">Selengkapnya</Link>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div class="card">
                            {/* <img src="{}" class="card-img-top" alt="..." /> */}
                            <div class="card-body">
                                <h5 class="card-title">Total Penjualan Bulan September</h5>
                                <h2 class="card-text"> 12</h2>
                                <a href="#" class="btn btn-primary">Selengkapnya</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-md-12" style={{marginTop: '20px',marginBottom : '50px'}}>
                <div class="card" style={{padding: '10px'}}>
                    <h4>Grafik Penjualan Tahun 2022 </h4>
                    <Bar data={datas} options={options} />
                </div>
            </div>
        </div>
    </div>
    );
  }
  
  export default HomeComponents;