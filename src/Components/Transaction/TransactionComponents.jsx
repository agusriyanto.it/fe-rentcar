import {Link, useNavigate} from 'react-router-dom'
import React, {useEffect, useState} from 'react'
import axios from 'axios'
import AuthUser, { API_URL } from '../../Utils/constants';
import { numberWithCommas } from '../../Utils/utils';
import ListCategoryComponent from './ListTransComponent';
import ResultTransComponent from './HasilTransComponent';
import ProdTransComponent from './ProdTransComponent';

const TransactionComponents = () => {
    const {API_URL} = AuthUser()
    const [data, setData] = useState([])
    const navigate = useNavigate()
    
    useEffect(() => {
        if(!localStorage.getItem('token')){
            navigate('/login')
        }
        API_URL.get('product')
            .then(response => {
                setData(response.data.data['a0']);
                console.log(data);
            })
            .catch(error => {
                console.error('Error Fetching Data : ', error);
            });
    }, []);

    return (
      <div className="container-fluid">
        <div className="row">
            <div className="col-md-2" style={{marginTop: '20px'}}>
                <ListCategoryComponent />
            </div>
            <div className="col-md-7" style={{marginTop: '20px'}}>
                {data.length > 0 ? data.map((item, index) => [
                    <ProdTransComponent 
                        key={item.id}
                        item={item}
                    />
                    ]) :
                    <div className="alert alert-danger mb-0">
                        Data Belum Tersedia!
                    </div>
                }
                
            </div>
            <div className="col-md-3" style={{marginTop: '20px'}}>
                <ResultTransComponent />
            </div>
        </div>
    </div>
    );
  }
  
  export default TransactionComponents;