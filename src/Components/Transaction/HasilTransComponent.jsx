import {Link, useHistory} from 'react-router-dom'
import swal from 'sweetalert2'

const ResultTransComponent = () => {
    const handleSubmit = (e) => {
        e.preventDefault()
        swal.fire({
            icon: 'info',
            title: "Perhatian",
            text: "Apakah data yang anda masukkan sudah benar?",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Ya, simpan',
            cancelButtonText: "Tidak, Batalkan"
        }).then((isConfirm)=>  {    
        if (isConfirm['isConfirmed']){
            // SavePost()
            swal.fire("Berhasil", "Terimakasih telah melakukan pembayaran", "success")
            e.preventDefault()
            } else {
            swal.fire("Dibatalkan", "Data anda tidak disimpan", "error")
            e.preventDefault()
            }
        })      
    }

    const handleSimpan = (e) => {
        e.preventDefault()
        swal.fire({
            icon: 'info',
            title: "Perhatian",
            text: "Apakah data yang anda masukkan sudah benar?",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Ya, simpan',
            cancelButtonText: "Tidak, Batalkan"
        }).then((isConfirm)=>  {    
        if (isConfirm['isConfirmed']){
            // SavePost()
            swal.fire("Berhasil", "Terimakasih telah melakukan pembayaran", "success")
            e.preventDefault()
            } else {
            swal.fire("Dibatalkan", "Data anda tidak disimpan", "error")
            e.preventDefault()
            }
        })      
    }

    return (
        <>
        <h5>List Pembelian</h5>
        <div className="row">
            <div className="col-md-12">
                <div className="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Qty</th>
                            <th scope="col">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>Kaos oblong Biru @50.000</td>
                                <td>5</td>
                                <td>250.000</td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>Kerudung Pasmina @35.000</td>
                                <td>10</td>
                                <td>350.000</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                Total Harga : <span className='font-weight-bold' >Rp. 600.000</span>
                <div className="row" style={{marginTop: '20px'}}>
                    <div className="col-md-6">
                        <a type="button" onClick={handleSubmit} class="btn btn-danger btn-block btn-sm" data-toggle="tooltip" data-placement="top" title="Bayar">
                            <i class="bi-cart3"> Bayar</i>
                        </a>
                    </div>
                    <div className="col-md-6">
                        <a type="button" onClick={handleSimpan} class="btn btn-danger btn-block btn-sm" data-toggle="tooltip" data-placement="top" title="Simpan">
                            <i class="bi-save"> Simpan</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        </>
        
    )
}

export default ResultTransComponent