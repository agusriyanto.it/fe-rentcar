import { Link } from 'react-router-dom';
import React, { useEffect, useState } from 'react';
import { API_URL } from '../../Utils/constants';
import { numberWithCommas } from '../../Utils/utils';
import axios from 'axios';

const ProdTransComponent = ({item}) => {

    return (
        <div class="row">
            <div class="col-6 col-md-4">
                <div class="card" style={{ width: '18rem', marginTop: '20px' }}>
                    <img src={"assets/img/sepatu1.jpg"} class="card-img-top"/>
                    <div class="card-body">
                        <h5 class="card-title">{item.prod_name}</h5>
                        <p class="card-text">Harga Rp. Rp. {numberWithCommas(item.prod_price)}</p>
                        <p class="card-text">Stok {item.prod_unit}</p>
                        <a href="#" class="btn btn-primary">Beli</a>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default ProdTransComponent
