import {Link,useNavigate} from 'react-router-dom'
import React, {useEffect, useState} from 'react'
import AuthUser, {API_URL} from '../../Utils/constants'
import axios from 'axios'
import swal from 'sweetalert2'

const ListTransComponent = () => {
    const {API_URL} = AuthUser()
    const [data, setData] = useState([])
    const navigate = useNavigate ()

    useEffect(() => {
        if(!localStorage.getItem('token')){
            navigate('/login')
        }
        API_URL.get('inv-caty/')
        .then(response => {
            setData(response.data.data['a0'])
            console.log(data)
        })
        .catch(error => {
            console.error('Error Fetching Data : ', error)
        })
    }, [])
  
    const showCategory = (id) => {
        API_URL.get('inv-caty' +id)
        .then(response => {
            setData(response.data.data['a0'])
            console.log(data)
        })
        .catch(error => {
            console.error('Error Fetching Data : ', error)
        })
    }

    const getCaty = (id) => {
        console.log(id)
    }
    return (
        <>
            <ul class="list-group shadow p-3 mb-5 bg-white rounded">
            {data.length > 0 ? data.map((item,index) => [                
                <a href="#" onChange={ getCaty(item.id) }><li class="list-group-item"> {item.inv_name} </li></a>
                
            ])
            : <tr>
                <td colSpan="4" className="text-center">
                    <div className="alert alert-danger mb-0">
                        Data Belum Tersedia!
                    </div>
                </td>
                </tr>
            }</ul>
        </>
             
    )
}

export default ListTransComponent