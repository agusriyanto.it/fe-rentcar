import axios from 'axios'
import {Link, useNavigate} from 'react-router-dom'
import AuthUser from '../Utils/constants'
import swal from 'sweetalert2'
import { useEffect, useState } from 'react'

const NavigasiComponent = () => {
    const {API_URL} = AuthUser()
    const navigate = useNavigate()
    const token = localStorage.getItem('token')
    const [user, setUser] = useState()

    useEffect(() => {
        getUser()
        
    })
    
    const getUser = () =>{
        axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
        API_URL.post('me')
        .then( (response) => {
            setUser(response.data)
            console.log(response.data)
        },100)
        .catch(error => {
            console.error('Failed get user :', error)
        })
    }

    const logoutHandler = (e) => {
        e.preventDefault()
        swal.fire({
            icon: 'info',
            title: "Perhatian",
            text: "Are you sure you logout of the application?",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes',
            cancelButtonText: "Cancel"
        }).then((isConfirm)=>  {    
            if (isConfirm['isConfirmed']){
                axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
                API_URL.post('/logout',{token : token})
                .then( () => {
                    localStorage.removeItem('token')
                    window.location.href = '/login'  
                },100)
                .catch(error => {
                    console.error('Galal Logout :', error)
                })
            } else {
              swal.fire("Canceled", "you cancel logout of the application", "error")
              e.preventDefault()
            }
        })
        
    }


    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <a className="navbar-brand" href="#">Rent Car Apps</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                <li className="nav-item active">
                    <Link className="nav-link" to="/">Home <span class="sr-only">(current)</span></Link>
                </li>
                <li className="nav-item active">
                    <Link className="nav-link" to="/category">Category <span class="sr-only">(current)</span></Link>
                </li>
                <li className="nav-item active">
                    <Link className="nav-link" to="/product">Unit <span class="sr-only">(current)</span></Link>
                </li>
                <li className="nav-item active">
                    <Link className="nav-link" to="/transaction">Transaction <span class="sr-only">(current)</span></Link>
                </li>
                </ul>
                <ul className="form-inline my-2 my-lg-0">
                    <a href="#" onClick={logoutHandler} className='text-white'>Logout</a>
                    <a className='text-white ml-3'>agus</a>
                </ul>
            </div>        
        </nav>
    )
}

export default NavigasiComponent
