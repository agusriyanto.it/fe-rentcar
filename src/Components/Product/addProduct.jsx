import {Link, useNavigate } from 'react-router-dom'
import React, {useEffect, useState} from 'react'
import AuthUser, {API_URL} from '../../Utils/constants'
import axios from 'axios'
import swal from 'sweetalert2'

const AddProductComponent = () => {
    const {API_URL} = AuthUser()
    const [data, setData] = useState([])
    const [host, setHost] = useState('')
    const [name, setName] = useState('')
    const [numb, setNumb] = useState('')
    const [unit, setUnit] = useState('')
    const [price, setPrice] = useState('')

    //useNavigate
    const navigate = useNavigate()

    const [errors, setErrors] = useState([])

    //method handle file change
    
    useEffect(() => {
      if(!localStorage.getItem('token')){
        navigate('/login')
      }
        API_URL.get('inv-caty')
        .then(response => {
            setData(response.data.data['a0'])
            console.log(data)
        })
        .catch(error => {
            console.error('Error Fetching Data : ', error)
        })
    }, [])

    
    const SavePost = () => {      
      const formData = new FormData()

      formData.append('host', host)
      formData.append('name', name)
      formData.append('unit', unit)
      formData.append('price', price)
      formData.append('numb', numb)
      
      API_URL.post('product',{
        name : name,
        host : host,
        numb : numb,
        price : price,
        unit : 1
      })
        .then(res => {
          swal.fire(
            'Berhasil',
            'Data anda berhasil disimpan',
            'success'
          )
          navigate('/product')
          console.log(res)
        })
        .catch(error => {
          swal.fire(
            'error!',
            'Data anda gagal disimpan, pastikan kolom bertanda bintang telah diisi...!',
            'error'
          )
            console.error('Error Fetching Data : ', error)
        })
      
    }

    const handleSubmit = (e) => {
      e.preventDefault()
      swal.fire({
        icon: 'info',
        title: "Perhatian",
        text: "Apakah data yang anda masukkan sudah benar?",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Ya, simpan',
        cancelButtonText: "Tidak, Batalkan"
      }).then((isConfirm)=>  {    
       if (isConfirm['isConfirmed']){
          SavePost()
        } else {
          swal.fire("Dibatalkan", "Data anda tidak disimpan", "error")
          e.preventDefault()
        }
     })
      
    }

    return (
      <div className="container text-left" style={{margin : '30px'}}>
        <div className="row">
            <div className="col-md-4" style={{marginTop: '20px'}}>
                <div className="card shadow p-3 mb-5 bg-white rounded" style={{width: '18rem'}}>
                    <div className="card-body">
                        <h5 className="card-title">Menu</h5>
                        <ul className="list-group">
                            <li className="list-group-item"><Link to="/" > Home </Link></li>
                            <li className="list-group-item"><Link to="/product" > Product</Link></li>
                            <li className="list-group-item"><Link to="/category" > Category </Link></li>
                            <li className="list-group-item"><Link to="/transaction" > Transaction </Link></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="col-md-8" style={{marginTop: '20px'}}>
              <div className="card shadow p-3 mb-5 bg-white rounded">
                <div className="card-body">
                  <h4>Tambah Data Prooduct</h4>
                  <form onSubmit={handleSubmit}>
                    <div className="form-group">
                      <label >Kategori</label>
                      <select className='form-control' onChange={(e) => setHost(e.target.value)} >
                        <option value="">Pilih Kategori...</option>
                        {data.map(option => (
                          <option value={option.id}>{option.inv_name}</option>
                        ))}
                      </select>
                        {
                            errors.host && (
                                <div className="alert alert-danger mt-2">
                                    {errors.title[0]}
                                </div>
                            )
                        }
                    </div>
                    <div className="form-group">
                      <label>Brand<span className='text-danger'> *</span></label>
                      <input type="text" className="form-control" onChange={(e) => setName(e.target.value)} placeholder="Brand"/>
                      {
                          errors.title && (
                              <div className="alert alert-danger mt-2">
                                  {errors.title[0]}
                              </div>
                          )
                      }
                    </div>
                    <div className="form-group">
                      <label>Police Number<span className='text-danger'> *</span></label>
                      <input type="text" className="form-control" onChange={(e) => setNumb(e.target.value)} placeholder="Police Number"/>
                      {
                          errors.title && (
                              <div className="alert alert-danger mt-2">
                                  {errors.title[0]}
                              </div>
                          )
                      }
                    </div>
                    <div className="form-group">
                      <label>Price<span className='text-danger'> *</span></label>
                      <input type="text" className="form-control" onChange={(e) => setPrice(e.target.value)} placeholder="Price"/>
                      {
                          errors.title && (
                              <div className="alert alert-danger mt-2">
                                  {errors.title[0]}
                              </div>
                          )
                      }
                    </div>
                    <button type="submit" className="btn btn-primary">Simpan</button>
                  </form>
                </div>
              </div>
            </div>
        </div>
    </div>
    );
  }
  
  export default AddProductComponent;