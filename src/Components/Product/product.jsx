import {Link, useNavigate} from 'react-router-dom'
import React, {useEffect, useState} from 'react'
import AuthUser, {API_URL} from '../../Utils/constants'
import {numberWithCommas} from '../../Utils/utils'
import axios from 'axios'
import swal from 'sweetalert2'

const ProductComponents = () => {
    const {API_URL} = AuthUser()
    const [data, setData] = useState([])
    const navigate = useNavigate ()

    useEffect(() => {
        if(!localStorage.getItem('token')){
            navigate('/login')
        }
        API_URL.get('product')
        .then(response => {
            setData(response.data.data['a0'])
            console.log(data)
        })
        .catch(error => {
            console.error('Error Fetching Data : ', error)
        })
    }, [])

    const deleteProduct = (id) => {
        swal.fire({
            title: 'Perhatian',
            text: "Apahakah anda yakin mau menghapus data ini?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Ya, Hapus',
            cancelButtonText: "Tidak, Batalkan"
        }).then((isConfrm) => {
            if(isConfrm['isConfirmed']){
                API_URL.delete('product/' + id )
                .then(() => {
                swal.fire({
                    title: 'Berhasil',
                    text: "Data anda telah berhasil dihapus",
                    icon: 'success',
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Ok',
                }).then((isConfrm) => {
                    if(isConfrm['isConfirmed']){
                    window.location.reload()
                    }
                })
                })
                .catch(error => {
                    console.error('Gagal Menghapus data :', error)
                })
            }else {
                swal.fire(
                'Dibatalkan',
                'Data anda berhasil diamankan',
                'info'
                )
            }
        })      
      }

    return (
        <div class="container text-left" style={{margin : '30px'}}>
            <div class="row">
                <div class="col-md-4" style={{marginTop: '20px'}}>
                    <div class="card shadow p-3 mb-5 bg-white rounded" style={{width: '18rem'}}>
                        {/* <img src="{}" class="card-img-top" alt="..." /> */}
                        <div class="card-body">
                            <h5 class="card-title">Menu</h5>
                            <ul class="list-group">
                                <li class="list-group-item"><Link to="/" > Home </Link></li>
                                <li class="list-group-item"><Link to="/product" > Product</Link></li>
                                <li class="list-group-item"><Link to="/category" > Category </Link></li>
                                <li class="list-group-item"><Link to="/transaction" > Transaction </Link></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-8" style={{marginTop: '20px'}}>
                    <div class="card shadow p-3 mb-5 bg-white rounded" style={{padding: '18px'}}>
                        <h4>Data Product</h4>
                        <Link to='/add-product' class='btn btn-info' style={{marginBottom: '20px'}}>Tambah Data Product</Link>
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Model</th>
                                <th scope="col">Kategori</th>
                                <th scope="col">Nomor Polisi</th>
                                <th scope="col">Harga Sewa</th>
                                <th scope="col" class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                {data.length > 0 ? data.map((item,index) => [
                                    <tr key={item.id}>
                                        <td>{index + 1 }</td>
                                        <td>{item.prod_brand}</td>
                                        <td>{item.inv_name}</td>
                                        <td>{item.prod_numb}</td>
                                        <td>Rp. {numberWithCommas(item.prod_price)}</td>
                                        <td class="text-center">
                                            <Link class="btn btn-info mr-3 mt-2"  to={'/edit-prod/'+ item.prod_id}><i class="bi-pencil"></i></Link>
                                            <a class="btn btn-danger mr-3 mt-2" type="button" onClick={() => deleteProduct(item.prod_id)}><i class="bi-trash"></i></a>
                                        </td>
                                    </tr>
                                ]) :   
                                <tr>
                                    <td colSpan="4" className="text-center">
                                        <div className="alert alert-danger mb-0">
                                            Data Belum Tersedia!
                                        </div>
                                    </td>
                                </tr>
                                }   
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
  }
  
  export default ProductComponents;