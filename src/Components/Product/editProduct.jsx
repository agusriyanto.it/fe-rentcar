import {Link, useNavigate, useParams } from 'react-router-dom'
import React, {useEffect, useState} from 'react'
import AuthUser, {API_URL} from '../../Utils/constants'
import axios from 'axios'
import swal from 'sweetalert2'

const EditProduct = () => {
    const {API_URL} = AuthUser()
    const [caty, setCaty] = useState([])
    const [host, setHost] = useState('')
    const [hostName, setHostName] = useState('')
    const [brand, setBrand] = useState('')
    const [numb, setNumb] = useState('')
    const [price, setPrice] = useState('')
    
    //useNavigate
    const navigate = useNavigate()

    const [errors, setErrors] = useState([])
    const {id} = useParams()
    //method handle file change
    
    useEffect(() => {
      if(!localStorage.getItem('token')){
        navigate('/login')
      }
        API_URL.get('product/'+ id)
        .then(response => {
            setHost(response.data.data['a0'][0].id)
            setHostName(response.data.data['a0'][0].inv_name)
            setBrand(response.data.data['a0'][0].prod_brand)
            setPrice(response.data.data['a0'][0].prod_price)
            setNumb(response.data.data['a0'][0].prod_numb)  
        })
        
        .catch(errors => {
            console.error('Error Fetching Data : ', errors)
        })
    }, [])

    const SavePost = () => {      
      API_URL.put('product/' + id ,{
        name : brand, 
        host : host,
        price : price,
        numb : numb
      })
        .then(res => {
          swal.fire(
            'Berhasil',
            'Data anda berhasil diedit',
            'success'
          )
          navigate('/product')
          console.log(res)
        })
        .catch(errors => {
          swal.fire(
            'error!',
            'Data anda tidak berhasil disimpan...!',
            'error'
          )
            console.error('Error Fetching Data : ', errors)
        })      
    }

    const handleSubmit = (e) => {
      e.preventDefault()
      swal.fire({
        icon: 'info',
        title: "Perhatian",
        text: "Apakah data yang akan anda edit sudah benar?",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Ya, Edit',
        cancelButtonText: "Tidak, Batalkan"
      }).then((isConfirm)=>  {    
       if (isConfirm['isConfirmed']){
          SavePost()
        } else {
          swal.fire("Dibatalkan", "Data anda tidak disimpan", "error")
          e.preventDefault()
        }
     })
      
    }

    return (
      <div className="container text-left" style={{margin : '30px'}}>
        <div className="row">
            <div className="col-md-4" style={{marginTop: '20px'}}>
                <div className="card shadow p-3 mb-5 bg-white rounded" style={{width: '18rem'}}>
                    <div className="card-body">
                        <h5 className="card-title">Menu</h5>
                        <ul className="list-group">
                            <li className="list-group-item"><Link to="/" > Home </Link></li>
                            <li className="list-group-item"><Link to="/product" > Product</Link></li>
                            <li className="list-group-item"><Link to="/category" > Category </Link></li>
                            <li className="list-group-item"><Link to="/transaction" > Transaction </Link></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="col-md-8" style={{marginTop: '20px'}}>
              <div className="card shadow p-3 mb-5 bg-white rounded">
                <div className="card-body">
                  <h4>Edit Data Product</h4>
                  <form onSubmit={handleSubmit} >
                    <div className="form-group">
                      <label>Model</label>
                      <select className='form-control' onChange={(e) => setHost(e.target.value)} >
                        <option value={host}>{hostName}</option>
                        {caty.map(option => (
                          <option value={option.id}>{option.inv_name}</option>
                        ))}
                      </select>
                      {
                          errors.name && (
                              <div className="alert alert-danger mt-2">
                                  {errors.host[0]}
                              </div>
                          )
                      }
                    </div>
                    <div className="form-group">
                      <label>Brand<span className='text-danger'> *</span></label>
                      <input type="text" className="form-control"  value={brand} onChange={(e) => setBrand(e.target.value)} placeholder="Brand"/>
                      {
                          errors.title && (
                              <div className="alert alert-danger mt-2">
                                  {errors.name[0]}
                              </div>
                          )
                      }
                    </div>
                    <div className="form-group">
                      <label>Police Number<span className='text-danger'> *</span></label>
                      <input type="number" className="form-control" value={numb} onChange={(e) => setNumb(e.target.value)} placeholder="Police Number"/>
                      {
                          errors.title && (
                              <div className="alert alert-danger mt-2">
                                  {errors.qty[0]}
                              </div>
                          )
                      }
                    </div>
                    <div className="form-group">
                      <label>Price<span className='text-danger'> *</span></label>
                      <input type="text" className="form-control" value={price} onChange={(e) => setPrice(e.target.value)} placeholder="Price"/>
                      {
                          errors.title && (
                              <div className="alert alert-danger mt-2">
                                  {errors.buy[0]}
                              </div>
                          )
                      }
                    </div>
                    <button type="submit" className="btn btn-primary">Edit</button>
                  </form>
                </div>
              </div>
            </div>
        </div>
    </div>
    );
  }
  
  export default EditProduct;