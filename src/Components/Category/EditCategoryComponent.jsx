import {Link, useNavigate, useParams } from 'react-router-dom'
import React, {useEffect, useState} from 'react'
import AuthUser, {API_URL} from '../../Utils/constants'
import axios from 'axios'
import swal from 'sweetalert2'

const EditCategoryComponent = () => {
    const {API_URL} = AuthUser()
    const [caty, setCaty] = useState([])
    const [host, setHost] = useState('')
    const [name, setName] = useState('')

    //useNavigate
    const navigate = useNavigate()

    const [errors, setErrors] = useState([])
    const {id} = useParams()
    //method handle file change
    
    useEffect(() => {
        API_URL.get('/inv-caty/'+ id)
        .then(response => {
            setHost(response.data.data['a0'].inv_host)
            setName(response.data.data['a0'].inv_name)
            setCaty(response.data.data['a1'])
        })
        .catch(errors => {
            console.error('Error Fetching Data : ', errors)
        })
    }, [])

    
    const SavePost = () => {      
      API_URL.put('/inv-caty/' + id ,{name : name, host : host})
        .then(res => {
          swal.fire(
            'Berhasil',
            'Data anda berhasil diedit',
            'success'
          )
          navigate('/category')
          console.log(res)
        })
        .catch(errors => {
          swal.fire(
            'error!',
            'Data anda tidak berhasil disimpan...!',
            'error'
          )
            console.error('Error Fetching Data : ', errors)
        })
      
    }

    const handleSubmit = (e) => {
      e.preventDefault()
      swal.fire({
        icon: 'info',
        title: "Perhatian",
        text: "Apakah data yang akan anda edit sudah benar?",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Ya, Edit',
        cancelButtonText: "Tidak, Batalkan"
      }).then((isConfirm)=>  {    
       if (isConfirm['isConfirmed']){
          SavePost()
        } else {
          swal.fire("Dibatalkan", "Data anda tidak disimpan", "error")
          e.preventDefault()
        }
     })
      
    }

    return (
      <div className="container text-left" style={{margin : '30px'}}>
        <div className="row">
            <div className="col-md-4" style={{marginTop: '20px'}}>
                <div className="card shadow p-3 mb-5 bg-white rounded" style={{width: '18rem'}}>
                    <div className="card-body">
                        <h5 className="card-title">Menu</h5>
                        <ul className="list-group">
                            <li className="list-group-item"><Link to="/" > Home </Link></li>
                            <li className="list-group-item"><Link to="/product" > Product</Link></li>
                            <li className="list-group-item"><Link to="/category" > Category </Link></li>
                            <li className="list-group-item"><Link to="/transaction" > Transaction </Link></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="col-md-8" style={{marginTop: '20px'}}>
              <div className="card shadow p-3 mb-5 bg-white rounded">
                <div className="card-body">
                  <h4>Edit Data Kategori</h4>
                  <form onSubmit={handleSubmit} >
                    <div className="form-group">
                      <label>Nama Kategori</label>
                      <input type="text" className="form-control" name='name' value={name} onChange={(e) => setName(e.target.value)} placeholder="Nama Kategori"/>
                      {
                          errors.name && (
                              <div className="alert alert-danger mt-2">
                                  {errors.name[0]}
                              </div>
                          )
                      }
                    </div>
                    <button type="submit" className="btn btn-primary">Edit</button>
                  </form>
                </div>
              </div>
            </div>
        </div>
    </div>
    );
  }
  
  export default EditCategoryComponent;