import {Link, useNavigate } from 'react-router-dom'
import React, {useEffect, useState} from 'react'
import AuthUser, {API_URL} from '../../Utils/constants'
import swal from 'sweetalert2'

const AddCategoryComponent = () => {
    const {API_URL} = AuthUser()
    const [data, setData] = useState([])
    const [name, setName] = useState('')
    const [validation, setValidation] = useState([])

    //useNavigate
    const navigate = useNavigate()

    //method handle file change
    
    useEffect(() => {
      if(!localStorage.getItem('token')){
        navigate('/login')
      }
        API_URL.get('/inv-caty')
        .then(response => {
            setData(response.data.data['a1'])
            console.log(data)
        })
        .catch(error => {
            console.error('Error Fetching Data : ', error)
        })
    }, [])

    
    const SavePost = () => {      
      const formData = new FormData()

      formData.append('name', name)
      formData.append('status', 2)
      API_URL.post('inv-caty',{name : name})
        .then(response => {
          swal.fire(
            'Berhasil',
            'Data anda berhasil disimpan',
            'success'
          )
          navigate('/category')
        })
        .catch(error => {
          swal.fire(
            'error!',
            'Data anda gagal disimpan, pastikan kolom bertanda bintang telah diisi...!',
            'error'
          )
            setValidation(error.response.data)
        })
      
    }

    const handleSubmit = (e) => {
      e.preventDefault()
      swal.fire({
        icon: 'info',
        title: "Perhatian",
        text: "Apakah data yang anda masukkan sudah benar?",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Ya, simpan',
        cancelButtonText: "Tidak, Batalkan"
      }).then((isConfirm)=>  {    
       if (isConfirm['isConfirmed']){
          SavePost()
        } else {
          swal.fire("Dibatalkan", "Data anda tidak disimpan", "error")
          e.preventDefault()
        }
     })
      
    }

    return (
      <div className="container text-left" style={{margin : '30px'}}>
        <div className="row">
            <div className="col-md-4" style={{marginTop: '20px'}}>
                <div className="card shadow p-3 mb-5 bg-white rounded" style={{width: '18rem'}}>
                    <div className="card-body">
                        <h5 className="card-title">Menu</h5>
                        <ul className="list-group">
                            <li className="list-group-item"><Link to="/" > Home </Link></li>
                            <li className="list-group-item"><Link to="/product" > Product</Link></li>
                            <li className="list-group-item"><Link to="/category" > Category </Link></li>
                            <li className="list-group-item"><Link to="/transaction" > Transaction </Link></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="col-md-8" style={{marginTop: '20px'}}>
              <div className="card shadow p-3 mb-5 bg-white rounded">
                <div className="card-body">
                  <h4>Tambah Data Kategori</h4>
                  <form onSubmit={handleSubmit}>
                    <div className="form-group">
                      <label>Nama Kategori<span className='text-danger'> *</span></label>
                      <input type="text" className="form-control" onChange={(e) => setName(e.target.value)} placeholder="Nama Kategori"/>
                      {
                          validation.name && (
                              <small className="text-danger mt-2">
                                  {validation.name[0]}
                              </small>
                          )
                      }
                    </div>
                    <button type="submit" className="btn btn-primary">Simpan</button>
                  </form>
                </div>
              </div>
            </div>
        </div>
    </div>
    );
  }
  
  export default AddCategoryComponent;