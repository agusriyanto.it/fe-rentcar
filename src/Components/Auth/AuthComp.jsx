import {Link, useHistory, useNavigate} from 'react-router-dom'
import React, {useEffect, useState} from 'react'
import AuthUser, {API_URL} from '../../Utils/constants'
import axios from 'axios'
import swal from 'sweetalert2'

const AuthComp = () => {
    const navigate = new useNavigate()
    const {API_URL} = AuthUser()
    const [email, setEmail] = useState()
    const [password, setPassword] = useState()
    const [validation, setValidation] = useState([])
    
    useEffect(() => {
        if(localStorage.getItem('token')){
            navigate('/')
        }
    }, [])

    const submitForm = (e)=>{
        e.preventDefault()
        API_URL.post('login',{email : email, password:password})
        .then(response => {
            localStorage.setItem('token',response.data.access_token)
            window.location.href = '/'            
            console.log(response.data)
        })
        .catch(error => {
            swal.fire(
                'error!',
                'Error, Please check data or fill form data',
                'error'
              )
            console.log(error.response.data)
            setValidation(error.response.data)
        })  
    }
  
    return (
        <div className='row justify-content-center pt-5'>
            <div className='col-sm-4'>
                <div className='card p-4'>
                    <form onSubmit={submitForm} >
                        {
                            validation.error && (
                                <div className="alert alert-danger" role="alert">
                                    {validation.error}
                                </div>
                            )
                        }
                        <div className='form-group'>
                            <label htmlFor="email">Email : </label>
                            <input type="email" className='form-control' placeholder='Input email' 
                                onChange={e=>setEmail(e.target.value)}
                            id='email' />
                            {
                            validation.email && (
                                <small className="text-danger mt-2">
                                    {validation.email[0]}
                                </small>
                            )
                            }
                        </div>
                        <div className='form-group mt-3'>
                            <label htmlFor="email">Password : </label>
                            <input type="password" className='form-control' placeholder='Input Password' 
                                onChange={e=>setPassword(e.target.value)}
                            id='password' />
                            {
                            validation.password && (
                                <small className="text-danger mt-2">
                                    {validation.password[0]}
                                </small>
                            )
                            }
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <button type='submit' className='btn btn-primary btn-block'>Login</button>
                            </div>
                            <div class="col-sm-4 text-right">
                                <Link to="/register" >Register Here</Link>
                            </div>
                        </div>                   
                    </form>

                </div>
            </div>
        </div>
    );
}
  
  export default AuthComp;