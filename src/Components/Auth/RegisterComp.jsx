import {Link, useHistory, useNavigate} from 'react-router-dom'
import React, {useEffect, useState} from 'react'
import AuthUser, {API_URL} from '../../Utils/constants'
import axios from 'axios'
import swal from 'sweetalert2'

const RegisterComp = () => {
    const navigate = new useNavigate()
    const {API_URL} = AuthUser()
    const [name, setName] = useState()
    const [email, setEmail] = useState()
    const [address, setAddress] = useState()
    const [wa, setWa] = useState()
    const [drivecard, setDrivecard] = useState()
    const [password, setPassword] = useState()
    const [confirm_password, setConfpassword] = useState()
    const [validation, setValidation] = useState([])
    
    useEffect(() => {
        if(localStorage.getItem('token')){
            navigate('/')
        }
    }, [])

    const submitRegister = (e)=>{
        e.preventDefault()
        API_URL.post('register',{
            name : name,
            email : email,
            address : address,
            wa : wa,
            drivecard : drivecard, 
            password: password,
            confirm_password : confirm_password
        })
        .then(response => {
            window.location.href = '/login'            
            console.log(response.data)
        })
        .catch(error => {
            swal.fire(
                'error!',
                'Error, Please check data or fill form data',
                'error'
            )
            console.log(error.response.data)
            setValidation(error.response.data)
        })  
    }
  
    return (
        <section class="vh-100" style={{marginTop : '10px'}}>
            <div class="container h-100">
                <div class="row d-flex justify-content-center align-items-cente">
                    <div class="col-lg-12 col-xl-11">
                        <div class="card text-black" style={{borderradius: '35px'}}>
                            <div class="card-body p-md-5">
                                <div class="row justify-content-center">
                                    <div class="col-md-10 col-lg-6 col-xl-5 order-2 order-lg-1">
                                        <p class="text-center h1 fw-bold mx-1 mx-md-4">Sign up</p>
                                        <form class="mx-1 mx-md-4" onSubmit={submitRegister}>
                                            <div class="d-flex flex-row align-items-center mb-4">
                                                <i class="fas fa-user fa-lg me-3 fa-fw"></i>
                                                <div class="form-outline flex-fill mb-0">
                                                    <label class="form-label" for="form3Example1c">Your Name</label>
                                                    <input type="text" class="form-control" onChange={e=>setName(e.target.value)} />
                                                    {
                                                        validation.name && (
                                                            <small className="text-danger mt-2">
                                                                {validation.name[0]}
                                                            </small>
                                                        )
                                                    }
                                                </div>
                                            </div>

                                            <div class="d-flex flex-row align-items-center mb-4">
                                                <i class="fas fa-envelope fa-lg me-3 fa-fw"></i>
                                                <div class="form-outline flex-fill mb-0">
                                                    <label class="form-label" for="form3Example3c">Your Email</label>
                                                    <input type="email" class="form-control" onChange={e=>setEmail(e.target.value)} />
                                                    {
                                                        validation.email && (
                                                            <small className="text-danger mt-2">
                                                                {validation.email[0]}
                                                            </small>
                                                        )
                                                    }
                                                </div>
                                            </div>

                                            <div class="d-flex flex-row align-items-center mb-4">
                                                <i class="fas fa-envelope fa-lg me-3 fa-fw"></i>
                                                <div class="form-outline flex-fill mb-0">
                                                    <label class="form-label" for="form3Example3c">Your Address</label>
                                                    <input type="text" class="form-control" onChange={e=>setAddress(e.target.value)} />
                                                    {
                                                        validation.address && (
                                                            <small className="text-danger mt-2">
                                                                {validation.address[0]}
                                                            </small>
                                                        )
                                                    }
                                                </div>
                                            </div>

                                            <div class="d-flex flex-row align-items-center mb-4">
                                                <i class="fas fa-envelope fa-lg me-3 fa-fw"></i>
                                                <div class="form-outline flex-fill mb-0">
                                                    <label class="form-label" for="form3Example3c">Phone Number</label>
                                                    <input type="text" class="form-control" onChange={e=>setWa(e.target.value)} />
                                                    {
                                                        validation.wa && (
                                                            <small className="text-danger mt-2">
                                                                {validation.wa[0]}
                                                            </small>
                                                        )
                                                    }
                                                </div>
                                            </div>

                                            <div class="d-flex flex-row align-items-center mb-4">
                                                <i class="fas fa-envelope fa-lg me-3 fa-fw"></i>
                                                <div class="form-outline flex-fill mb-0">
                                                    <label class="form-label" for="form3Example3c">Driving License</label>
                                                    <input type="text" class="form-control" onChange={e=>setDrivecard(e.target.value)} />
                                                    {
                                                        validation.drivecard && (
                                                            <small className="text-danger mt-2">
                                                                {validation.drivecard[0]}
                                                            </small>
                                                        )
                                                    }
                                                </div>
                                            </div>
                                            
                                            <div class="d-flex flex-row align-items-center mb-4">
                                                <i class="fas fa-lock fa-lg me-3 fa-fw"></i>
                                                <div class="form-outline flex-fill mb-0">
                                                    <label class="form-label" for="form3Example4c">Password</label>
                                                    <input type="password" class="form-control" onChange={e=>setPassword(e.target.value)} />
                                                    {
                                                        validation.password && (
                                                            <small className="text-danger mt-2">
                                                                {validation.password[0]}
                                                            </small>
                                                        )
                                                    }
                                                </div>
                                            </div>

                                            <div class="d-flex flex-row align-items-center mb-4">
                                                <i class="fas fa-key fa-lg me-3 fa-fw"></i>
                                                <div class="form-outline flex-fill mb-0">
                                                    <label class="form-label" for="form3Example4cd">Confirm password</label>
                                                    <input type="password" class="form-control" onChange={e=>setConfpassword(e.target.value)} />
                                                    {
                                                        validation.confirm_password && (
                                                            <small className="text-danger mt-2">
                                                                {validation.confirm_password[0]}
                                                            </small>
                                                        )
                                                    }
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <button type="submit" class="btn btn-primary btn-sm btn-block">Register</button>
                                                </div>
                                                <div class="col-sm-4 text-right">
                                                    <Link to="/login" >Login</Link> 
                                                </div>
                                            </div>    
                                        </form>
                                    </div>
                                    <div class="col-md-10 col-lg-6 col-xl-7 d-flex align-items-center order-1 order-lg-2">
                                        <img src={"https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-registration/draw1.webp"} class="img-fluid"/>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        // <div className='row justify-content-center pt-5'>
        //     <div className='col-sm-4'>
        //         <h5>Register User</h5>
        //         <div className='card p-4'>
        //             <form onSubmit={submitForm} >
        //                 {
        //                     validation.error && (
        //                         <div className="alert alert-danger" role="alert">
        //                             {validation.error}
        //                         </div>
        //                     )
        //                 }
        //             <div className='form-group'>
        //                 <label htmlFor="email">Email : </label>
        //                 <input type="email" className='form-control' placeholder='Input email' 
        //                     onChange={e=>setEmail(e.target.value)}
        //                 id='email' />
        //                 {
        //                   validation.email && (
        //                       <small className="text-danger mt-2">
        //                           {validation.email[0]}
        //                       </small>
        //                   )
        //                 }
        //             </div>
        //             <div className='form-group mt-3'>
        //                 <label htmlFor="email">Password : </label>
        //                 <input type="password" className='form-control' placeholder='Input Password' 
        //                     onChange={e=>setPassword(e.target.value)}
        //                 id='password' />
        //                 {
        //                   validation.password && (
        //                       <small className="text-danger mt-2">
        //                           {validation.password[0]}
        //                       </small>
        //                   )
        //                 }
        //             </div>
        //             <div className='form-group mt-3'>                        
        //                 <div class="d-flex flex-row-reverse bd-highlight">
        //                     <div class="p-2 bd-highlight">
        //                         <Link to="/category" >Register Here</Link>
        //                     </div>
        //                 </div>
        //             </div>
        //             <button type='submit' className='btn btn-primary mt-4'>Login</button>
        //             </form>

        //         </div>
        //     </div>
        // </div>
    );
}
  
  export default RegisterComp;